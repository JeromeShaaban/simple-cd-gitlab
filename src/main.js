import { TemplateLoader } from "./_helpers/template-loader"
import { PeopleService } from "./services/people-service"

class Main {
    
    #peoples = []

    // PeopleService
    #service = null
    
    constructor() {
        this.#service = new PeopleService()
        this.#peoples = this.#service.peoples
        this.#run()
    }

    static #makeCheckboxTd() {
        const td = document.createElement("td")
        const templateLoader = new TemplateLoader("item-checkbox")
        try {
            const checkbox = templateLoader.loadTemplate()
            td.appendChild(checkbox)
        } catch(e) {
            td.innerHTML = "&nbsp;"
        }
        return td
    }

    get peoples() {
        return this.#peoples
    }

    #run() {
        const title = document.querySelector('h1')
        title.innerText = 'JS'

        // Add an element in the DOM
        const titleLevel2 = document.createElement("h2")
        titleLevel2.innerText = "Titre niveau 2"

        // Hook the new DOM element to an existing element
        document.body.appendChild(titleLevel2)

        const tbody = document.querySelector("tbody")
        this.#peoples.forEach((people) => {
            const tr = document.createElement("tr")

            // Add tr for the future checkbox using a static method
            tr.appendChild(Main.#makeCheckboxTd())

            // Go through each people object to read each attribute
            for (const attribute in people) {
                const td = document.createElement("td")
                td.innerText = people[attribute]
                tr.appendChild(td)
            }
            tbody.appendChild(tr)
        })
    }
}

// Self callable function to run the Main class
let app
(function () {
    app = new Main()
})()

// Event listener forthe main checkbox
document.getElementById("main-checkbox").addEventListener(
    "click", (event) => {
        const checkbox = event.target
        const itemCheckboxes = document.getElementsByClassName("item-checkbox")

        let checkTheBox = false

        if (checkbox.checked) {
            checkTheBox = true
        }
        
        for (const itemCheckbox of itemCheckboxes) {
            itemCheckbox.checked = checkTheBox
        }
    }
)

const tbody = document.querySelector("tbody")
tbody.addEventListener(
    "click", (event) => {
        if (event.target.tagName === "INPUT") {
            const checkbox = event.target

            // Ensure we act on the good checkbox
            if (checkbox.classList.contains("item-checkbox")) {
                const mainCheckbox = document.getElementById("main-checkbox")
                if (checkbox.checked === false) {
                    mainCheckbox.checked = false
                } else {
                    const itemCheckboxes = Array.from(document.getElementsByClassName("item-checkbox"))
                    const checkedItems = itemCheckboxes.filter((itemCheckbox) => itemCheckbox.checked)

                    /* Si la soustraction des deux longueurs est égale à zéro, comme zéro est ici considéré comme false, et que le "!" inverse le résultat, on passera le state de 
                    mainCheckbox.checked à "inverse de false" = true */
                    mainCheckbox.checked = !(checkedItems.length - app.peoples.length)
                }
            }
        }
    }
)